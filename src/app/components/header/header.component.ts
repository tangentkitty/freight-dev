import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuItems = [
    { label: 'Solutions', link: '#' },
    { label: 'Customer Stories', link: '#' },
    { label: 'Blog', link: '#' },
    { label: 'Contact', link: '#' }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
