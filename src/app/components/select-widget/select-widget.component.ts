import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-widget',
  templateUrl: './select-widget.component.html',
  styleUrls: ['./select-widget.component.scss']
})
export class SelectWidgetComponent implements OnInit {

  originLabel = 'Origin Port or Warehouse';
  destinationLabel = 'Destination Port or Warehouse';
  date = '26 Nov 2019';
  capacity = 'LCL';
  quantity = 'Qty';

  constructor() { }

  ngOnInit(): void {
  }

}
